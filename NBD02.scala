
object NBD02 {
  def main(args: Array[String]) {
    println("Cwiczenia 2")
    val days = List("Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela")

    //1 Pattern Matching
    def checkDay(day: String) : String = day match {
      case "Poniedziałek" | "Wtorek" | "Środa" | "Czwartek" | "Piątek" => "Praca"
      case "Sobota" | "Niedziela" => "Weekend"
      case _ => "Nie ma takiego dnia"
    }

    //2 Konto Bankowe
    class KontoBankowe(stanKonta: Double) {
       private var _stanKonta2 = stanKonta

      def this() {
        this(0)
      }

      def wplata(kwota: Double): Unit = {
        _stanKonta2 += kwota
      }
      def wyplata(kwota: Double): Unit = {
        _stanKonta2 -= kwota
      }

      def getStanKonta = _stanKonta2
    }
    
    case class Person(name: String, surname: String) {
      def sayHello = this match {
          case Person(name, surname) => s"Cześć $name $surname"
          case _ => "Dzień dobry"
      }
    }

    //4
    def funcjaLiczb(liczba: Int, funkcja: (Int) => Int): Int = {
      funkcja(funkcja(funkcja(liczba)))
    }

    //5
    abstract class Osoba(val imie: String, val nazwisko: String) {
      def podatek: Double
    }

    trait Student extends Osoba {
      override def podatek = 0
    }

    trait Pracownik extends Osoba {
      override def podatek = pensja * 0.2
      var pensja = 0.0

      def getPensja(): Double = {
        this.pensja
      }

      def setPensja(nowaPensja: Double): Unit = {
        this.pensja = nowaPensja
      }
    }

    trait Nauczyciel extends Pracownik {
      override def podatek = pensja * 0.1
    }

    println("zadanie 1: " + checkDay("Piątek"))

    //2
    var konto1 = new KontoBankowe(2000)
    konto1.wplata(1000)
    konto1.wyplata(500)
    println("Zadanie 2: Stan konta " + konto1.getStanKonta)

    //3
    var osoba1 = new Person("Julia", "Nowak")
    var osoba2 = new Person("Michał", "Kowalski")
    var osoba3 = new Person("Zosia", "Iksińska")

    println(osoba1.sayHello)
    println(osoba2.sayHello)
    println(osoba3.sayHello)

    //4
    val x = 15
    def plusOne(num: Int): Int = num + 1
    println("Zadanie 4: " + funcjaLiczb(x, plusOne))

    //5
    println("Zadanie 5:")
    var pracownikNauczyciel = new Osoba("Julia", "Nowak") with Pracownik with Nauczyciel
    pracownikNauczyciel.setPensja(100)
    println("pracownik-nauczyciel: " + pracownikNauczyciel.podatek)
    var nauczycielPracownik = new Osoba("Michał", "Kowalski") with Nauczyciel with Pracownik
    nauczycielPracownik.setPensja(100)
    println("nauczyciel-pracownik: " + nauczycielPracownik.podatek)
    var studentPracownik = new Osoba("Zosia", "Iksińska") with Student with Pracownik
    studentPracownik.setPensja(100)
    println("student-pracownik: " + studentPracownik.podatek)
    var pracownikStudent = new Osoba("Zosia", "Iksińska") with Pracownik with Student
    pracownikStudent.setPensja(100)
    println("pracownik-student: " + pracownikStudent.podatek)
  }
}