import scala.annotation.tailrec

object NBD01 {
  def main(args: Array[String]) {
    println("Cwiczenia 1")
    val days = List("Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela")

    //1a Pętla for
    def getElements(list: List[String]) : String = {
      var result = list.head;
      for (value <- list.tail)
        result += ", " + value;
      result
    }

    //1b Pętla for wypisująca tylko dni z nazwami zaczynającymi się na "P"
    def getElementsWith(start: String, list: List[String]) : String = {
      val filteredDays = list.filter(_.startsWith(start))
      var result = filteredDays.head;
      for (element <- filteredDays.tail)
        result += ", " + element;

      result
    }

    //1c Pętla while
    def getElementsUsingWhile(list: List[String]) : String = {
      var current = list
      var result = ""
      while(!current.isEmpty) {
        result += current.head + ", "
        current = current.tail
      };
      result
    }

    //2a Funkcja rekurencyjna
    def getElementsUsingRecursion(list: List[String]) : String = list match {
      case Nil => ""
      case head :: tail => head + ", " + getElementsUsingRecursion(tail)
    }

    //2b Funkcja rekurencyjna wypisująca elementy listy od końca
    def getElementsUsingRecursionBackwards(list: List[String]) : String = list match {
      case head :: Nil => head
      case head :: tail => getElementsUsingRecursionBackwards(tail) + ", " + head
      case Nil => ""
    }

    //3 funkcja ogonowa z dodatkowym stringem
    @tailrec def getElementsWithExtra(s: String, list: List[String]) : String = list match {
        case head :: Nil => s + ", " + head
        case head :: tail => getElementsWithExtra(s + ", " + head, tail)
        case Nil => ""
    }

    //4a Metoda foldl
    def getElementsUsingFoldL(list: List[String]) : String = list.tail.foldLeft(list.head) {
      ((left, right) => left + ", " + right)
    }

    //4b Metoda foldr
    def getElementsUsingFoldR(list: List[String]) : String = list.init.foldRight(list.last) {
      ((left, right) => left + ", " + right)
    }

    //4c Metoda foldl wypisująca tylko dni z nazwami zaczynającymi się na „P”
    def getElementsUsingFoldLWith(list: List[String]) : String = getElementsUsingFoldL(list.filter({_.startsWith("P")}))

    //5 Stwórz mapę z nazwami produktów i cenami. Na jej podstawie wygeneruj drugą, z 10% obniżką cen.
    val products = Map("A" -> 10, "B" -> 20, "C" -> 30, "D" -> 40)
    def discountedProducts = products.map(_._2 * 0.9)

    //6 Zdefiniuj funkcję przyjmującą krotkę z 3 wartościami różnych typów i wypisującą je
    def tuple[A, B, C](t: Tuple3[A, B, C]) = t._1 + ", " + t._2 + ", " + t._3

    //7 Option
    val capitals = Map("France" -> "Paris", "Japan" -> "Tokyo")
    def show(x: Option[String]) = x match {
      case Some(s) => s
      case None => "?"
    }

    //8 Napisz funkcję usuwającą zera z listy wartości całkowitych
    def remove(n: Int, list: List[Int]): List[Int] = list match {
      case Nil => Nil
      case head :: tail =>
        if (head == n)
          remove(n, tail)
        else
          head :: remove(n, tail)
    }
    val list = List(0, 15, 1, 2, 4, 0, 4, 2, 1, 0)

    //9 Zwiększ liczby z listy o 1
    def increaseBy(x: Int, list: List[Int]): List[Int] = list.map(x+_)

    //10 Stwórz funkcję przyjmującą listę liczb rzeczywistych i zwracającą stworzoną na jej podstawie listę zawierającą wartości bezwzględne elementów z oryginalnej listy należących do przedziału <-5,12>.
    def getABS(list: List[Double]): List[Double] = list.filter(n => n >= -5 && n <= 12).map(_.abs)
    val list2 = List(3.0, 16.0, 70.0, 91.0)

    println("zadanie 1a: " + getElements(days))
    println("zadanie 1b: " + getElementsWith("P", days))
    println("zadanie 1c: " + getElementsUsingWhile(days))
    println("zadanie 2a: " + getElementsUsingRecursion(days))
    println("zadanie 2b: " + getElementsUsingRecursionBackwards(days))
    println("zadanie 3: " + getElementsWithExtra("EXTRA", days))
    println("zadanie 4a: " + getElementsUsingFoldL(days))
    println("zadanie 4b: " + getElementsUsingFoldR(days))
    println("zadanie 4c: " + getElementsUsingFoldLWith(days))
    println("zadanie 5: " + discountedProducts)
    println("zadanie 6: " + tuple(3, "Sarna", 9.0))
    println("Zadanie 7: Japan : " + show(capitals.get( "Japan")))
    println("Zadanie 7: India : " + show(capitals.get( "India")))
    println("zadanie 8: " + remove(0, list))
    println("zadanie 9: " + increaseBy(1, list))
    println("zadanie 10: " + getABS(list2))
  }


}
