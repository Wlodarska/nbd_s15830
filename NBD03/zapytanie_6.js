use('nbd');

//Dodaj siebie do bazy, zgodnie z formatem danych użytych dla innych osób (dane dotyczące karty kredytowej, adresu zamieszkania i wagi mogą być fikcyjne);
db.people.insert({
  sex: 'Female',
  first_name: 'Patrycja',
  last_name: 'Włodarska',
  job: 'Programmer',
  email: 's15830@pjwstk.edu.pl',
  location: {
    city: 'City',
    address: { streetname: 'Street', streetnumber: '1' }
  },
  description: "I am cool",
  height: 176,
  weight: 60,
  birth_date: '1997-06-05T00:00:11Z',
  nationality: 'Poland',
  credit: [
    {
      type: 'Visa',
      number: '1234567890',
      currency: 'PLN',
      balance: '100'
    }
  ]
});
